let appointmentList = [];

class Api {
    constructor(requestType = 'GET', route = '', prefix = 'https://ori.clow.nl/algds/') {
        this.requestType = requestType;
        this.route = route;
        this.prefix = prefix;
    }

    execute(successCallbackFunction) {
        var xHttp = new XMLHttpRequest();
        xHttp.onreadystatechange = function () {
            if (xHttp.readyState == 4 && xHttp.status == 200) {
                var response = JSON.parse(xHttp.response);
                successCallbackFunction(response);
            }
        };
        xHttp.open(this.requestType, this.prefix + this.route, true);
        xHttp.send();
    }
}

function getAppointments() {
    var myApi = new Api('GET', 'defaultAfspraken.json');
    myApi.execute(showAppointments, errorAppointments);
}

function addAppointment() {
    var date = document.getElementById("gewenstTijdstipDate").value;
    var time = document.getElementById("gewenstTijdstipTime").value;
    var DateTime = date + " " + time;

    var formInput =
    {
        "Id": appointmentList.lastIndexOf++,
        "naamKlant": document.getElementById("name").value,
        "adresKlant": document.getElementById("address").value,
        "gewenstTijdstip": formatDate(new Date(DateTime)),
        "dichtsbijzijndeHalte": document.getElementById("closestStop").value,
        "afstandHalte": document.getElementById("distance").value,
        "redenAfspraak": document.getElementById("reason").value,
        "naamMonteur": document.getElementById("mechanic").value,
    };

    appointmentList.push(formInput);
    document.getElementById("appointmentForm").reset();
    createTable(appointmentList);
}

function formatDate(gewenstTijdstip) {
    var date = new Date(gewenstTijdstip);

    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();

    if (hh < 10) {
        hh = '0' + hh;
    }
    if (min < 10) {
        min = '0' + min;
    }

    return dd + "-" + mm + "-" + yyyy + " " + hh + ":" + min + ":00";
}

function showAppointments(list) {
    for (let i = 0; i < list.length; i++) {
        appointmentList.push(list[i].Afspraak);
        appointmentList[i].gewenstTijdstip = Date.parse(appointmentList[i].gewenstTijdstip);
    }
    bubbleSort(appointmentList);
    createTable(appointmentList);
};

function bubbleSort(array) {
    var swapped;
    do {
        swapped = false;
        for (var i = 0; i < array.length - 1; i++) {
            if (array[i].gewenstTijdstip > array[i + 1].gewenstTijdstip) {
                var temp = array[i].gewenstTijdstip;
                array[i].gewenstTijdstip = array[i + 1].gewenstTijdstip;
                array[i + 1].gewenstTijdstip = temp;
                swapped = true;
            }
        }
    } while (swapped);

    for (var i = 0; i < array.length; i++) {
        var date = new Date(array[i].gewenstTijdstip);
        array[i].gewenstTijdstip = date.toLocaleString();
    }
    return array;
}

function getSearchCriteria(buttonId) {
    // get search criteria
    var result = [];
    var searchCriteria = document.getElementById("search-appointment").value;
    var clickedButton = buttonId;

    // optional validation here


    // call the right search function
    if (clickedButton === "monteur") {
        result = searchMechanicAppointments(searchCriteria);
    } else if (clickedButton === "klant") {
        result = searchCustomerAppointments(searchCriteria);
    }

    // check if the search result contains a match and
    // call function to generate a table with the results data
    // or show an appropriate message if there are no matches
    if (result.length >= 1) {
        createTable(result);
    } else {
        window.alert("no matches found message placeholder");
    }
}

function searchCustomerAppointments(name) {
    var newArray = [];
    var customerName = name.toLowerCase();

    for (var i = 0; i < appointmentList.length; i++) {
        var currentCustomer = appointmentList[i].naamKlant.toLowerCase();
        if (customerName == currentCustomer) {
            newArray.push(appointmentList[i]);
        }
    }

    return newArray;
}

function searchMechanicAppointments(name) {
    var newArray = [];
    var mechanicName = name.toLowerCase();

    for (var i = 0; i < appointmentList.length; i++) {
        var currentMechanic = appointmentList[i].naamMonteur.toLowerCase();
        if (mechanicName == currentMechanic) {
            newArray.push(appointmentList[i]);
        }
    }

    return newArray;
}

function errorAppointments(err) {
    console.log(err);
}

function sortByName() {
    var sortedArray = quicksortNames(appointmentList);
    createTable(sortedArray);
}

function quicksortNames(array) {
    if (array.length <= 1) {
        return array;
    }

    var pivot = array[0];
    var lesserArray = [];
    var greaterArray = [];

    for (var i = 1; i < array.length; i++) {
        if (array[i].naamKlant >= pivot.naamKlant) {
            greaterArray.push(array[i]);
        } else {
            lesserArray.push(array[i]);
        }
    }

    return quicksortNames(lesserArray).concat(pivot, quicksortNames(greaterArray));
}

function createTable(list) {
    var cols = [];
    var table = document.createElement("table");
    var trow = table.insertRow(-1);

    for (var i = 0; i < list.length; i++) {
        for (var key in list[i]) {
            if (cols.indexOf(key) === -1) {
                cols.push(key);
            }
        }
    }

    for (var i = 0; i < cols.length; i++) {
        var theader = document.createElement("th");     // TABLE HEADER.
        switch (cols[i]) {
            case "naamKlant":
                theader.innerHTML = "Naam klant";
                break;
            case "adresKlant":
                theader.innerHTML = "Adres";
                break;
            case "gewenstTijdstip":
                theader.innerHTML = "Gewenst tijdstip";
                break;
            case "dichtsbijzijndeHalte":
                theader.innerHTML = "Dichtsbijzijnde halte";
                break;
            case "afstandHalte":
                theader.innerHTML = "Afstand halte";
                break;
            case "redenAfspraak":
                theader.innerHTML = "Reden";
                break;
            case "naamMonteur":
                theader.innerHTML = "Naam monteur";
                break;
        }
        trow.appendChild(theader);
    }

    for (var i = 0; i < list.length; i++) {
        trow = table.insertRow(-1);
        for (var j = 0; j < cols.length; j++) {
            var cell = trow.insertCell(-1);
            cell.innerHTML = list[i][cols[j]];
        }
    }

    let rows = table.rows;
    for (var i = 0; i < rows.length; i++) {
        rows[i].deleteCell(0);
    }

    var tableContainer = document.getElementById("appointmentTable");
    tableContainer.innerHTML = "";
    tableContainer.appendChild(table);
}

function hideAllPages() {
    var homePage = document.getElementById('home-page');
    var routingPage = document.getElementById('routing-page');
    homePage.style.display = 'none';
    routingPage.style.display = 'none';
}

function showHomePage() {
    var page = document.getElementById('home-page');
    hideAllPages();
    page.style.display = 'block';
}

function showRoutingPage() {
    var page = document.getElementById('routing-page');
    hideAllPages();
    page.style.display = 'block';
}

function importMap(fileName) {

    var xmlhttp = new XMLHttpRequest();
    var url = fileName;

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

const LOG_DETAILS = 1;
const LOG_STEPS = 2;
const LOG_ERRORS = 3;
const LOG_LEVEL = LOG_DETAILS;

var totalNumberOfFiles = 0;

var totalNumberOfAppointmentFiles = 0;

/**
 * Function that onyl writes info to the console if its message exceeds the general LOG LEVEL
 * @param {*} msg 
 * @param {*} msgSeverity (one of the values LOG_DETAILS, LOG_STEPS, LOG_ERRORS)
 */
function logInfo(msg, msgSeverity) {
    if (msgSeverity >= LOG_LEVEL) {
        // log
    }
}

/**
 * ImportMap is a function that read the content from a URL and expects JSON in return.
 * It then hands over the content to the function processMap to actually process its content
 * 
 * @param {*} fileName 
 */
function importMap(fileName) {

    var xmlhttp = new XMLHttpRequest();
    var url = fileName;

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            processMapInfo(myArr);

            totalNumberOfFiles++;
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

/**
 * Reads the line information and updates the internal map structure
 * @param {*} lineData (JSON string as object)
 */
function processMapInfo(lineData) {
    var mainItem;
    var networkItem;
    var networkItemStop;
    var listOfStops = [];

    for (mainItem in lineData) {
        logInfo(mainItem, LOG_DETAILS);

        for (networkItem in lineData[mainItem]["Network"]) {
            for (networkItemStop in lineData[mainItem]["Network"][networkItem]) {
                if (!listOfStops.hasOwnProperty(lineData[mainItem]["Network"][networkItem][networkItemStop].UserStopOrderNumber)) {
                    listOfStops[lineData[mainItem]["Network"][networkItem][networkItemStop].UserStopOrderNumber] =
                        lineData[mainItem]["Network"][networkItem][networkItemStop].TimingPointName;
                }
            }
        }
    }

    logInfo(listOfStops, LOG_DETAILS);

    for (var i = 0; i < listOfStops.length; i++) {
        var stopList = document.getElementById("closestStop")
        var opt = listOfStops[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        stopList.appendChild(el);
    };
}

/**
 * We need a way to check that ALL imports have finished as this is a ASYNCHRONOUS process
 * @param {*} expectedNumberOfFiles 
 */
function checkImport(expectedNumberOfFiles) {
    if (totalNumberOfFiles == expectedNumberOfFiles) {
        clearTimeout();

        // etc... all JSON files have been processed ... do your thing :-)
    }
}

/**
 * ImportAppointments is a function that reads the content from a URL and expects JSON in return.
 * It then hands over the content to the function processAppointments to actually process its content
 * 
 * @param {*} fileName 
 */
function importAppointments(fileName) {

    var xmlhttp = new XMLHttpRequest();
    var url = fileName;

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            processAppointments(myArr);
            // increase the counter so that function checkImportAppointments() can check that we loaded all appointment files
            totalNumberOfAppointmentFiles++;
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

/**
* Reads the line information and updates the internal map structure
* @param {*} lineData (JSON string as object)
*/
function processAppointments(listOfAppoinments) {
    var mainItem;
    var customerName;

    for (mainItem in listOfAppoinments) {
        logInfo(mainItem, LOG_DETAILS);

        /*  We have now reached details of the 1st appointment
            Just read the details ....
         */
        customerName = listOfAppoinments[mainItem]["Afspraak"].naamKlant;

        // etc... read more details from this appointment...
    }
}

/**
* We need a way to check that ALL imports have finished as this is a ASYNCHRONOUS process
* @param {*} expectedNumberOfFiles 
*/
function checkImportAppointments(expectedNumberOfFiles) {
    if (totalNumberOfAppointmentFiles == expectedNumberOfFiles) {
        clearTimeout();

        // etc... all JSON files have been processed ... do your thing :-)
    }
}

function init() {
    showHomePage();
    importMap("https://ori.clow.nl/algds/GVB_1_1.json");
    importMap("https://ori.clow.nl/algds/GVB_2_1.json");
    importMap("https://ori.clow.nl/algds/GVB_3_1.json");
    importMap("https://ori.clow.nl/algds/GVB_4_1.json");
    importMap("https://ori.clow.nl/algds/GVB_5_1.json");
    importMap("https://ori.clow.nl/algds/GVB_7_1.json");
    importMap("https://ori.clow.nl/algds/GVB_11_1.json");
    importMap("https://ori.clow.nl/algds/GVB_12_1.json");
    importMap("https://ori.clow.nl/algds/GVB_13_1.json");
    importMap("https://ori.clow.nl/algds/GVB_14_1.json");
    importMap("https://ori.clow.nl/algds/GVB_17_1.json");
    importMap("https://ori.clow.nl/algds/GVB_19_1.json");
    importMap("https://ori.clow.nl/algds/GVB_24_1.json");
    importMap("https://ori.clow.nl/algds/GVB_26_1.json");
    importMap("https://ori.clow.nl/algds/GVB_50_1.json");
    importMap("https://ori.clow.nl/algds/GVB_51_1.json");
    importMap("https://ori.clow.nl/algds/GVB_52_1.json");
    importMap("https://ori.clow.nl/algds/GVB_53_1.json");
    importMap("https://ori.clow.nl/algds/GVB_54_1.json");
}

init();